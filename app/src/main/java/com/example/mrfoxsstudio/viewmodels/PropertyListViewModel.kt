package com.example.mrfoxsstudio.viewmodels

import android.graphics.drawable.Drawable
import android.opengl.Visibility
import android.view.View
import android.widget.Button
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.PropertyListFragment
import com.example.mrfoxsstudio.PropertyListFragmentDirections
import com.example.mrfoxsstudio.R
import com.example.mrfoxsstudio.data.Property
import com.example.mrfoxsstudio.data.PropertyRepository
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PropertyListViewModel @Inject internal constructor(
    val propertyRepository: PropertyRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val properties: LiveData<List<Property>> = propertyRepository.getProperties().asLiveData()


    init {

    }

    fun change(view: View, button: Button, button1: FloatingActionButton) {
        viewModelScope.launch {

            propertyRepository.getPropertyCount().collect {

                if (it == 0) {
                    button1.visibility = View.GONE
                    button.visibility = View.VISIBLE
                } else {
                    button1.visibility = View.VISIBLE
                    button.visibility = View.GONE
                }
            }
        }
    }

}
