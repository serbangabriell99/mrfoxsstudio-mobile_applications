package com.example.mrfoxsstudio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.adapters.PropertyAdapter
import com.example.mrfoxsstudio.databinding.FragmentPropertyListBinding
import com.example.mrfoxsstudio.viewmodels.PropertyListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PropertyListFragment : Fragment() {
    private var _binding: FragmentPropertyListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: PropertyListViewModel by viewModels()

    @InternalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPropertyListBinding.inflate(inflater, container, false)
        context ?: return binding.root

        viewModel.change(binding.root, binding.addPropertyButton, binding.floatingActionButton)

        val adapter = PropertyAdapter()
        binding.propertyList.adapter = adapter
        subscribeUi(adapter)

        binding.floatingActionButton.setOnClickListener {
            val direction =
                PropertyListFragmentDirections.actionPropertyListFragmentToPropertyEditFragment(-1L)
            findNavController().navigate(direction)
        }
        binding.addPropertyButton.setOnClickListener {
            val direction =
                PropertyListFragmentDirections.actionPropertyListFragmentToPropertyEditFragment(-1L)
            findNavController().navigate(direction)
        }

        binding.topAppBar.setNavigationOnClickListener {
            binding.drawerLayout.open()
        }

        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_properties -> binding.drawerLayout.close()
                R.id.nav_profile -> findNavController().navigate(R.id.action_property_list_fragment_to_settings_fragment)
            }
            true
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun subscribeUi(adapter: PropertyAdapter) {
        viewModel.properties.observe(viewLifecycleOwner) { property ->
            adapter.submitList(property)
        }
    }


}
