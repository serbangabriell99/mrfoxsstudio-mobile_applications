package com.example.mrfoxsstudio.viewmodels

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.data.*
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TenantEditViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val tenantRepository: TenantRepository
) : ViewModel() {
    val tenantId: Long = savedStateHandle.get<Long>(TENANT_ID_SAVED_STATE_KEY)!!
    private val propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> get() = _name
    private val _email = MutableLiveData<String>()
    val email: LiveData<String> get() = _email
    private val _phone = MutableLiveData<String>()
    val phone: LiveData<String> get() = _phone
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    init {
        if (tenantId != -1L) {
            viewModelScope.launch {
                tenantRepository.getTenant(tenantId).collect { tenant ->
                    if (tenant != null) {
                        _name.postValue(tenant.name)
                        _email.postValue(tenant.email)
                        _phone.postValue(tenant.phone)
                    }
                }
            }
        }

    }

    fun onSaveClick(view: View) {
        viewModelScope.launch {

            if (_name.value == null || _name.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Tenant's Name", 1000)
                mySnackbar.show()
                return@launch
            }

            if (_email.value == null || _email.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Tenant's Email", 1000)
                mySnackbar.show()
                return@launch
            }

            if (_phone.value == null || _phone.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Tenant's Phone Number", 1000)
                mySnackbar.show()
                return@launch
            }
            if (!(_email.value!!.matches(emailPattern.toRegex()))) {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add a valid email address", 1000)
                mySnackbar.show()
                return@launch
            }

            val tenant = Tenant(
                _name.value!!,
                _email.value!!,
                _phone.value!!,
                propertyId
            )
            if (tenantId == -1L) {
                tenantRepository.createTenant(
                    tenant.name,
                    tenant.propertyId,
                    tenant.email,
                    tenant.phone
                )
            } else {
                tenantRepository.updateTenant(
                    tenant.also { it.tenantId = tenantId })
            }

            view.findNavController().popBackStack()

            hideKeyboard(view)
        }
    }

    fun onDeleteTenant(view: View) {
        viewModelScope.launch {
            if (tenantId != -1L) {
                tenantRepository.removeTenant(Tenant("", "", "", -1L).also {
                    it.tenantId = tenantId
                })
                view.findNavController().popBackStack()
                hideKeyboard(view)
            }
        }
    }

    fun onNameChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _name.postValue(s.toString())
    }

    fun onEmailChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _email.postValue(s.toString())
    }

    fun onPhoneChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _phone.postValue(s.toString())
    }

    companion object {
        private const val TENANT_ID_SAVED_STATE_KEY = "tenantId"
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}