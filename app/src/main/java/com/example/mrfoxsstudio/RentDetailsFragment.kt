package com.example.mrfoxsstudio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.databinding.FragmentRentDetailsBinding
import com.example.mrfoxsstudio.databinding.FragmentRentEditBinding
import com.example.mrfoxsstudio.viewmodels.RentDetailsViewModel
import com.example.mrfoxsstudio.viewmodels.RentEditViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RentDetailsFragment : Fragment() {
    private var _binding: FragmentRentDetailsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: RentDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentRentDetailsBinding>(
            inflater,
            R.layout.fragment_rent_details,
            container,
            false
        ).apply {
            viewModel = this@RentDetailsFragment.viewModel
            lifecycleOwner = viewLifecycleOwner

            floatingActionButton.setOnClickListener {
                val direction =
                    PropertyDetailFragmentDirections.actionPropertyDetailFragmentToRentEditFragment(
                        (viewModel as RentDetailsViewModel).propertyId
                    )
                findNavController().navigate(direction)
            }

        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}