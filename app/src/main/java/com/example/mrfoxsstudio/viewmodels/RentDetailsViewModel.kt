package com.example.mrfoxsstudio.viewmodels

import android.view.View
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.PropertyDetailFragmentDirections
import com.example.mrfoxsstudio.data.PropertyRepository
import com.example.mrfoxsstudio.data.RentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RentDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val rentRepository: RentRepository
) : ViewModel() {
    val propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!

    val rent = rentRepository.getRent(propertyId).asLiveData()
    val format = SimpleDateFormat("dd-MM-yyyy", Locale.UK)

    companion object {
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }

}