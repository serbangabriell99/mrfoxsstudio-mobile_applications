package com.example.mrfoxsstudio.data

import androidx.room.*

@Entity(
    tableName = "tenants",
    indices = [Index("property_id")],
    foreignKeys = [ForeignKey(
        entity = Property::class,
        parentColumns = ["id"],
        childColumns = ["property_id"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class Tenant(
    val name: String,
    val email: String,
    val phone: String,
    @ColumnInfo(name = "property_id") val propertyId: Long
) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var tenantId: Long = 0
}
