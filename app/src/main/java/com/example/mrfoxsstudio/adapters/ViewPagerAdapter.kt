package com.example.mrfoxsstudio.adapters

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mrfoxsstudio.*

const val PROPERTY_PAGE_INDEX = 0
const val TENANT_PAGE_INDEX = 1
const val RENT_PAGE_INDEX = 2

class ViewPagerAdapter(private val propertyDetailFragment: PropertyDetailFragment) :
    FragmentStateAdapter(propertyDetailFragment) {

    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        PROPERTY_PAGE_INDEX to {
            OpenListFragment().apply {
                arguments = bundleOf("propertyId" to propertyDetailFragment.propertyId)
            }
        },
        TENANT_PAGE_INDEX to {
            TenantTabFragment().apply {
                arguments = bundleOf("propertyId" to propertyDetailFragment.propertyId)
            }
        },
        RENT_PAGE_INDEX to {
            RentEditFragment().apply {
                arguments = bundleOf("propertyId" to propertyDetailFragment.propertyId)
            }
        })

    override fun getItemCount() = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment {
        return tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
    }


}