package com.example.mrfoxsstudio.viewmodels

import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.PropertyDetailFragmentDirections
import com.example.mrfoxsstudio.data.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OpenListViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val propertyRepository: PropertyRepository
) : ViewModel() {
    val propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!

    val property = propertyRepository.getProperty(propertyId).asLiveData()

    companion object {
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }

    fun onClick(view: View) {
        val direction =
            PropertyDetailFragmentDirections.actionPropertyDetailFragmentToPropertyEditFragment(
                propertyId
            )
        view.findNavController().navigate(direction)
    }
}