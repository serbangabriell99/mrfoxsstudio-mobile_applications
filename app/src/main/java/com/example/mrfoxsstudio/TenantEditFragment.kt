package com.example.mrfoxsstudio

import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.data.*
import com.example.mrfoxsstudio.databinding.FragmentPropertyEditBinding
import com.example.mrfoxsstudio.databinding.FragmentPropertyListBinding
import com.example.mrfoxsstudio.databinding.FragmentTenantEditBinding
import com.example.mrfoxsstudio.viewmodels.PropertyEditViewModel
import com.example.mrfoxsstudio.viewmodels.TenantEditViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TenantEditFragment : Fragment() {
    private var _binding: FragmentTenantEditBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TenantEditViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentTenantEditBinding>(
            inflater,
            R.layout.fragment_tenant_edit,
            container,
            false
        ).apply {
            viewModel = this@TenantEditFragment.viewModel
            lifecycleOwner = viewLifecycleOwner

            buttonDeleteTenant.setOnClickListener()
            {
                view?.let { it1 -> showAlertDialog(it1) }
            }

            screen.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }
        }

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().popBackStack()
            view?.let { it1 -> hideKeyboard(it1) }
        }

        if (viewModel.tenantId != -1L) {
            binding.buttonDeleteTenant.visibility = View.VISIBLE
            binding.buttonAddTenant.text = "Save Tenant"
        } else {
            binding.buttonDeleteTenant.visibility = View.GONE
            binding.buttonAddTenant.text = "Add Tenant"
        }

        return binding.root
    }

    fun showAlertDialog(view: View) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Delete")
            .setMessage("Are you sure you want to delete the tenant?")
            .setNegativeButton("No") { dialog, which ->
                showSnackbar("The tenant was not deleted", view)
            }
            .setPositiveButton("Yes") { dialog, which ->
                viewModel.onDeleteTenant(view)
            }.show()
    }

    fun showSnackbar(msg: String, view: View) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}