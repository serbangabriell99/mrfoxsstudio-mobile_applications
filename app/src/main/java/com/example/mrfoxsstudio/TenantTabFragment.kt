package com.example.mrfoxsstudio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.adapters.TenantAdapter
import com.example.mrfoxsstudio.databinding.FragmentTenantTabBinding
import com.example.mrfoxsstudio.databinding.FragmentWelcomeBinding
import com.example.mrfoxsstudio.viewmodels.TenantTabViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TenantTabFragment : Fragment() {
    private var _binding: FragmentTenantTabBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TenantTabViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTenantTabBinding.inflate(inflater, container, false)
        context ?: return binding.root
        val adapter = TenantAdapter()
        binding.recyclerView.adapter = adapter
        subscribeUi(adapter)

        binding.floatingActionButton.setOnClickListener {
            val direction =
                PropertyDetailFragmentDirections.actionPropertyDetailFragmentToTenantEditFragment(
                    viewModel.propertyId,
                    -1L
                )
            findNavController().navigate(direction)
        }
        binding.addTenantButton.setOnClickListener {
            val direction =
                PropertyDetailFragmentDirections.actionPropertyDetailFragmentToTenantEditFragment(
                    viewModel.propertyId,
                    -1L
                )
            findNavController().navigate(direction)
        }

        viewModel.change(
            binding.root,
            binding.addTenantButton,
            binding.floatingActionButton,
            binding.foxOnButton
        )

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun subscribeUi(adapter: TenantAdapter) {
        viewModel.tenants.observe(viewLifecycleOwner) { tenant ->
            adapter.submitList(tenant)
        }
    }


}