package com.example.mrfoxsstudio

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.databinding.FragmentPropertyEditBinding
import com.example.mrfoxsstudio.viewmodels.PropertyEditViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PropertyEditFragment : Fragment() {
    private var _binding: FragmentPropertyEditBinding? = null
    private val binding get() = _binding!!
    private val viewModel: PropertyEditViewModel by viewModels()


    override fun onResume() {
        super.onResume()
        val items = resources.getStringArray(R.array.property_types)
        val adapter =
            ArrayAdapter(requireContext(), R.layout.list_item_property_type, items)
        binding.type.setAdapter(adapter)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentPropertyEditBinding>(
            inflater,
            R.layout.fragment_property_edit,
            container,
            false
        ).apply {
            viewModel = this@PropertyEditFragment.viewModel
            lifecycleOwner = viewLifecycleOwner


            buttonDeleteProperty.setOnClickListener()
            {
                view?.let { it1 -> showAlertDialog(it1) }
                view?.let { it1 -> hideKeyboard(it1) }
            }

            screen.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }
            type.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }
        }

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().popBackStack()
            view?.let { it1 -> hideKeyboard(it1) }
        }

        if (viewModel.propertyId != -1L) {
            binding.buttonDeleteProperty.visibility = View.VISIBLE
            binding.addButton.text = "Save Property"
        } else {
            binding.buttonDeleteProperty.visibility = View.GONE
            binding.addButton.text = "Add Property"
        }


        return binding.root
    }

    fun showAlertDialog(view: View) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Delete")
            .setMessage(
                "Are you sure you want to delete the entire property? This includes tenants," +
                        " property and rent details."
            )
            .setNegativeButton("No") { dialog, which ->
                showSnackbar("The property was not deleted", view)
            }
            .setPositiveButton("Yes") { dialog, which ->
                viewModel.onDeleteProperty(view)
            }.show()
    }

    fun showSnackbar(msg: String, view: View) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}