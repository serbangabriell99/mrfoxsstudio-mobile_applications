package com.example.mrfoxsstudio.di;

import android.content.Context
import com.example.mrfoxsstudio.data.AppDatabase
import com.example.mrfoxsstudio.data.PropertyDao
import com.example.mrfoxsstudio.data.RentDao
import com.example.mrfoxsstudio.data.TenantDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun providePropertyDao(appDatabase: AppDatabase): PropertyDao {
        return appDatabase.propertyDao()
    }

    @Provides
    fun provideTenantDao(appDatabase: AppDatabase): TenantDao {
        return appDatabase.tenantDao()
    }

    @Provides
    fun provideRentDao(appDatabase: AppDatabase): RentDao {
        return appDatabase.rentDao()
    }
}
