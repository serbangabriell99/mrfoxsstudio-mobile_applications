package com.example.mrfoxsstudio.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TenantDao {
    @Delete
    suspend fun deleteTenant(tenant: Tenant)

    @Transaction
    @Query("SELECT * FROM properties WHERE id IN (SELECT DISTINCT(property_id) FROM tenants)")
    fun getPropertiesWithTenants(): Flow<List<PropertyWithTenants>>

    @Query("SELECT * FROM tenants WHERE id = :tenantId")
    fun getTenant(tenantId: Long): Flow<Tenant?>

    @Query("SELECT * FROM tenants WHERE property_id = :propertyId")
    fun getTenants(propertyId: Long): Flow<List<Tenant>>

    @Query("SELECT COUNT(id) FROM tenants")
    fun getTenantCount(): Flow<Int>

    @Insert
    suspend fun insertTenant(tenant: Tenant): Long

    @Update
    suspend fun updateTenant(tenant: Tenant)
}
