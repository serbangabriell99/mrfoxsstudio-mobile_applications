package com.example.mrfoxsstudio.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface RentDao {
    @Delete
    suspend fun deleteRent(rent: Rent)

    @Query("SELECT * FROM rents WHERE id = :propertyId")
    fun getRent(propertyId: Long): Flow<Rent?>

    @Insert
    suspend fun insertRent(rent: Rent): Long

    @Update
    suspend fun updateRent(rent: Rent)

    @Query("SELECT * FROM properties")
    @Transaction
    fun getPropertiesAndRents(): Flow<List<PropertyAndRent>>
}