package com.example.mrfoxsstudio.data

import android.widget.AutoCompleteTextView
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "properties")
data class Property(
    val address: String,
    val name: String,
    val type: String,
    val postcode: String,
) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var propertyId: Long = 0
}
