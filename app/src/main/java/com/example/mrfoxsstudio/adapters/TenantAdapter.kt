package com.example.mrfoxsstudio.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mrfoxsstudio.PropertyDetailFragmentDirections
import com.example.mrfoxsstudio.PropertyListFragmentDirections
import com.example.mrfoxsstudio.data.Property
import com.example.mrfoxsstudio.data.Tenant
import com.example.mrfoxsstudio.databinding.ListItemPropertyBinding
import com.example.mrfoxsstudio.databinding.ListItemTenantBinding

class TenantAdapter : ListAdapter<Tenant, RecyclerView.ViewHolder>(TenantDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TenantViewHolder(
            ListItemTenantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val tenant = getItem(position)
        (holder as TenantViewHolder).bind(tenant)
    }

    class TenantViewHolder(private val binding: ListItemTenantBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener { view ->
                binding.tenant?.let { tenant ->
                    val direction =
                        PropertyDetailFragmentDirections.actionPropertyDetailFragmentToTenantEditFragment(
                            tenant.propertyId,
                            tenant.tenantId
                        )
                    view.findNavController().navigate(direction)
                }
            }
        }

        fun bind(item: Tenant) {
            binding.apply {
                tenant = item
                executePendingBindings()
            }
        }
    }
}

private class TenantDiffCallback : DiffUtil.ItemCallback<Tenant>() {
    override fun areItemsTheSame(oldItem: Tenant, newItem: Tenant) =
        oldItem.tenantId == newItem.tenantId

    override fun areContentsTheSame(oldItem: Tenant, newItem: Tenant) = oldItem == newItem
}
