package com.example.mrfoxsstudio

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.adapters.PROPERTY_PAGE_INDEX
import com.example.mrfoxsstudio.adapters.RENT_PAGE_INDEX
import com.example.mrfoxsstudio.adapters.TENANT_PAGE_INDEX
import com.example.mrfoxsstudio.adapters.ViewPagerAdapter
import com.example.mrfoxsstudio.databinding.FragmentPropertyDetailBinding
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import java.lang.IndexOutOfBoundsException

@AndroidEntryPoint
class PropertyDetailFragment : Fragment() {

    private var _binding: FragmentPropertyDetailBinding? = null
    private val binding get() = _binding!!
    private var _propertyId: Long = -1L
    val propertyId get() = _propertyId
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { _propertyId = it.getLong("propertyId") }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPropertyDetailBinding.inflate(inflater, container, false)

        binding.viewPager.adapter = ViewPagerAdapter(this)

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = getTabTitle(position)
            tab.setIcon(getTabIcon(position))
        }.attach()

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().popBackStack()
            view?.let { it1 -> hideKeyboard(it1) }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            PROPERTY_PAGE_INDEX -> "Details"
            TENANT_PAGE_INDEX -> getString(R.string.tenants)
            RENT_PAGE_INDEX -> "Rent"
            else -> null
        }
    }

    private fun getTabIcon(position: Int): Int {
        return when (position) {
            PROPERTY_PAGE_INDEX -> R.drawable.ic_baseline_house_24
            TENANT_PAGE_INDEX -> R.drawable.fox_smaller
            RENT_PAGE_INDEX -> R.drawable.ic_baseline_attach_money_24
            else -> throw IndexOutOfBoundsException()
        }
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}