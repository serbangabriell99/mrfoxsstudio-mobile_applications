buildscript {
    val kotlin_version by extra("1.5.21")
    dependencies {
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:2.3.5")
        classpath("com.android.tools.build:gradle:4.2.2")
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.38")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.21")
    }
    repositories {
        google()
        mavenCentral()
    }
}

tasks {
    register("clean", Delete::class) {
        delete(rootProject.buildDir)
    }
}
