package com.example.mrfoxsstudio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.mrfoxsstudio.databinding.FragmentOpenListBinding
import com.example.mrfoxsstudio.viewmodels.OpenListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OpenListFragment : Fragment() {

    private var _binding: FragmentOpenListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: OpenListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentOpenListBinding>(
            inflater,
            R.layout.fragment_open_list,
            container,
            false
        ).apply {
            viewModel = this@OpenListFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}