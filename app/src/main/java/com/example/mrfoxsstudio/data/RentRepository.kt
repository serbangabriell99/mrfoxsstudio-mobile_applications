package com.example.mrfoxsstudio.data

import android.widget.CheckBox
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RentRepository @Inject constructor(
    private val rentDao: RentDao
) {
    suspend fun createRent(
        propertyId: Long,
        rent: String,
        rentDue: Calendar,
        rentEnd: Calendar,
        rented: Boolean,
        currency: String
    ) {
        val rent = Rent(propertyId, rent, rentDue, rentEnd, rented, currency)
        rentDao.insertRent(rent)
    }

    fun getRent(propertyId: Long) = rentDao.getRent(propertyId)

    suspend fun removeRent(rent: Rent) {
        rentDao.deleteRent(rent)
    }

    suspend fun updateRent(rent: Rent) {
        rentDao.updateRent(rent)
    }
}