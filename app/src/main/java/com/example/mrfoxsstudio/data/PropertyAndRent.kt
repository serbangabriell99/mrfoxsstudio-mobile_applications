package com.example.mrfoxsstudio.data

import androidx.room.Embedded
import androidx.room.Relation

data class PropertyAndRent(
    @Embedded val property: Property,
    @Relation(parentColumn = "id", entityColumn = "id") val rent: Rent
)