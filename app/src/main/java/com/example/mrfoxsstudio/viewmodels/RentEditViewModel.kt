package com.example.mrfoxsstudio.viewmodels

import android.content.Context
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.Checkable
import android.widget.CompoundButton
import android.widget.Toast
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.R
import com.example.mrfoxsstudio.data.*
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RentEditViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val rentRepository: RentRepository
) : ViewModel() {
    val propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!
    private var hasRent = false

    private val _rent = MutableLiveData<String>()
    val rent: LiveData<String> get() = _rent
    private val _rentDue = MutableLiveData<Calendar>()
    val rentDue: LiveData<Calendar> get() = _rentDue
    private val _rentDueText = MutableLiveData<String>()
    val rentDueText: LiveData<String> get() = _rentDueText
    private val _rentEnd = MutableLiveData<Calendar>()
    val rentEnd: LiveData<Calendar> get() = _rentEnd
    private val _rentEndText = MutableLiveData<String>()
    val rentEndText: LiveData<String> get() = _rentEndText
    private val _rented = MutableLiveData<Boolean>()
    val rented: LiveData<Boolean> get() = _rented
    private val _currency = MutableLiveData<String>()
    val currency: LiveData<String> get() = _currency

    private val rentNumbers = "[0-9]"

    init {
        if (propertyId != -1L) {
            viewModelScope.launch {
                rentRepository.getRent(propertyId).collect { rent ->
                    if (rent != null) {
                        hasRent = true
                        _rent.postValue(rent.rent)
                        _rentDue.postValue(rent.rentDue)
                        val myFormat = "dd-MM-yyyy"
                        val sdf = SimpleDateFormat(myFormat, Locale.UK)
                        _rentDueText.postValue(sdf.format(rent.rentDue.time))
                        _rentEnd.postValue(rent.rentEnd)
                        val sdf1 = SimpleDateFormat(myFormat, Locale.UK)
                        _rentEndText.postValue(sdf1.format(rent.rentEnd.time))
                        _rented.postValue(rent.rented)
                        _currency.postValue(rent.currency)
                    }
                }
            }
        }

    }

    fun onSaveClick(view: View) {
        viewModelScope.launch {
            // TODO: Validate form

            if (_rent.value == null || _rent.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please fill Monthly Rent Price", 1000)
                mySnackbar.show()
                return@launch
            }
            if (_currency.value == null) {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please pick a Currency", 1000)
                mySnackbar.show()
                return@launch
            }
            if (_rentDue.value == null) {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please fill Tenancy Start Date", 1000)
                mySnackbar.show()
                return@launch
            }
            if (_rentEnd.value == null) {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please fill Tenancy End Date", 1000)
                mySnackbar.show()
                return@launch
            }
            if (_rented.value == null) {
                _rented.value = false
            }

            val rent = Rent(
                propertyId,
                _rent.value!!,
                _rentDue.value!!,
                _rentEnd.value!!,
                _rented.value!!,
                _currency.value!!

            )
            if (hasRent) {

                rentRepository.updateRent(
                    rent
                )
            } else {
                rentRepository.createRent(
                    rent.propertyId,
                    rent.rent,
                    rent.rentDue,
                    rent.rentEnd,
                    rent.rented,
                    rent.currency
                )
            }

            val mySnackbar = Snackbar.make(view, "Saved", 600)
            mySnackbar.show()

            hideKeyboard(view)

        }
    }

    fun onDeleteRent(view: View) {
        viewModelScope.launch {
            if (hasRent) {
                rentRepository.removeRent(
                    Rent(
                        propertyId,
                        "",
                        Calendar.getInstance(),
                        Calendar.getInstance(),
                        false,
                        ""
                    )
                )
                view.findNavController().popBackStack()
                hideKeyboard(view)
            } else {
                val mySnackbar = Snackbar.make(view, "Nothing saved to delete", 1000)
                mySnackbar.show()
            }
        }
    }

    fun onRentChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val value = s.toString()
        if (value == null) {
            _rent.postValue(_rent.value)
        } else {
            _rent.postValue(value!!)
        }

    }

    fun onRentDueChanged(calendar: Calendar) {
        _rentDue.postValue(calendar)
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        _rentDueText.postValue(sdf.format(calendar.time))
    }

    fun onRentEndChanged(calendar: Calendar) {
        _rentEnd.postValue(calendar)
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        _rentEndText.postValue(sdf.format(calendar.time))
    }

    fun onRentedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        _rented.postValue(isChecked)
    }

    fun onCurrencyChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _currency.postValue(s.toString())
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    companion object {
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }
}