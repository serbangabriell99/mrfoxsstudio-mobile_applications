package com.example.mrfoxsstudio.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface PropertyDao {
    @Delete
    suspend fun deleteProperty(property: Property)

    @Query("SELECT * FROM properties")
    fun getProperties(): Flow<List<Property>>

    @Query("SELECT * FROM properties WHERE id = :propertyId")
    fun getProperty(propertyId: Long): Flow<Property?>

    @Query("SELECT COUNT(id) FROM properties")
    fun getPropertyCount(): Flow<Int>

    @Insert
    suspend fun insertProperty(property: Property): Long

    @Update
    suspend fun updateProperty(property: Property)
}
