package com.example.mrfoxsstudio.data

import android.widget.AutoCompleteTextView
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PropertyRepository @Inject constructor(private val propertyDao: PropertyDao) {
    suspend fun createProperty(postcode: String, address: String, name: String, type: String) {
        val property = Property(address, name, type, postcode)
        propertyDao.insertProperty(property)
    }

    fun getProperties() = propertyDao.getProperties()

    fun getProperty(propertyId: Long) = propertyDao.getProperty(propertyId)

    fun getPropertyCount() = propertyDao.getPropertyCount()

    suspend fun removeProperty(property: Property) {
        propertyDao.deleteProperty(property)
    }

    suspend fun updateProperty(property: Property) {
        propertyDao.updateProperty(property)
    }
}
