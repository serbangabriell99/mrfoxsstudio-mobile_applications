package com.example.mrfoxsstudio.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mrfoxsstudio.PropertyListFragmentDirections
import com.example.mrfoxsstudio.data.Property
import com.example.mrfoxsstudio.databinding.ListItemPropertyBinding

class PropertyAdapter : ListAdapter<Property, RecyclerView.ViewHolder>(PropertyDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PropertyViewHolder(
            ListItemPropertyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val property = getItem(position)
        (holder as PropertyViewHolder).bind(property)
    }

    class PropertyViewHolder(private val binding: ListItemPropertyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener { view ->
                binding.property?.let { property ->
                    val direction =
                        PropertyListFragmentDirections.actionPropertyListFragmentToPropertyDetailFragment(
                            property.propertyId
                        )
                    view.findNavController().navigate(direction)
                }
            }
        }

        fun bind(item: Property) {
            binding.apply {
                property = item
                executePendingBindings()
            }
        }
    }
}

private class PropertyDiffCallback : DiffUtil.ItemCallback<Property>() {
    override fun areItemsTheSame(oldItem: Property, newItem: Property) =
        oldItem.propertyId == newItem.propertyId

    override fun areContentsTheSame(oldItem: Property, newItem: Property) = oldItem == newItem
}
