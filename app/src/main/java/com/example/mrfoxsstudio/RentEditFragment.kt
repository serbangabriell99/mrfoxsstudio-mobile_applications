package com.example.mrfoxsstudio

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.R
import com.example.mrfoxsstudio.adapters.TenantAdapter
import com.example.mrfoxsstudio.databinding.FragmentPropertyEditBinding
import com.example.mrfoxsstudio.databinding.FragmentRentEditBinding
import com.example.mrfoxsstudio.databinding.FragmentTenantEditBinding
import com.example.mrfoxsstudio.databinding.FragmentTenantTabBinding
import com.example.mrfoxsstudio.viewmodels.RentEditViewModel
import com.example.mrfoxsstudio.viewmodels.TenantEditViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class RentEditFragment : Fragment() {
    private var _binding: FragmentRentEditBinding? = null
    private val binding get() = _binding!!
    private val viewModel: RentEditViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentRentEditBinding>(
            inflater,
            R.layout.fragment_rent_edit,
            container,
            false
        ).apply {
            viewModel = this@RentEditFragment.viewModel
            lifecycleOwner = viewLifecycleOwner


            val datePicker = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                (viewModel as RentEditViewModel).onRentDueChanged(Calendar.getInstance().apply {
                    set(Calendar.YEAR, year)
                    set(Calendar.MONTH, month)
                    set(Calendar.DAY_OF_MONTH, dayOfMonth)
                })
            }

            val datePicker2 = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                (viewModel as RentEditViewModel).onRentEndChanged(Calendar.getInstance().apply {
                    set(Calendar.YEAR, year)
                    set(Calendar.MONTH, month)
                    set(Calendar.DAY_OF_MONTH, dayOfMonth)
                })
            }

            buttonDatePicker.setOnClickListener()
            {
                view?.let { it1 -> hideKeyboard(it1) }
                val calendar =
                    (viewModel as RentEditViewModel).rentDue.value ?: Calendar.getInstance()
                DatePickerDialog(
                    this@RentEditFragment.requireContext(),
                    datePicker,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }

            buttonDatePicker2.setOnClickListener()
            {
                view?.let { it1 -> hideKeyboard(it1) }
                val calendar =
                    (viewModel as RentEditViewModel).rentDue.value ?: Calendar.getInstance()
                DatePickerDialog(
                    this@RentEditFragment.requireContext(),
                    datePicker2,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }

            checkbox.setOnCheckedChangeListener(viewModel!!::onRentedChanged)

            val items = resources.getStringArray(R.array.currencies)
            val adapter =
                ArrayAdapter(requireContext(), R.layout.list_item_property_type, items)
            (currency.editText as? AutoCompleteTextView)?.setAdapter(adapter)

            buttonDeleteTenant.setOnClickListener {
                view?.let { it1 -> showAlertDialog(it1) }
                view?.let { it1 -> hideKeyboard(it1) }

            }

            screen.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }

            currencyDrop.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }
            checkbox.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }

            currency.setOnClickListener {
                view?.let { it1 -> hideKeyboard(it1) }
            }
        }
        return binding.root
    }

    fun showAlertDialog(view: View) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Delete").setMessage("Are you sure you want to delete all rent details?")
            .setNegativeButton("No") { dialog, which ->
                showSnackbar("The details were not deleted", view)
            }
            .setPositiveButton("Yes") { dialog, which ->
                viewModel.onDeleteRent(view)
            }.show()
    }

    fun showSnackbar(msg: String, view: View) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}