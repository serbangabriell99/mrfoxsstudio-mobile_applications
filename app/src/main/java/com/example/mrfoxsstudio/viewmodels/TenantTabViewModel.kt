package com.example.mrfoxsstudio.viewmodels

import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.lifecycle.*
import com.example.mrfoxsstudio.data.Property
import com.example.mrfoxsstudio.data.PropertyRepository
import com.example.mrfoxsstudio.data.Tenant
import com.example.mrfoxsstudio.data.TenantRepository
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TenantTabViewModel @Inject internal constructor(
    val tenantRepository: TenantRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!
    val propertyId get() = _propertyId
    val tenants: LiveData<List<Tenant>> = tenantRepository.getTenants(_propertyId).asLiveData()

    init {
    }

    companion object {
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }

    fun change(view: View, button: Button, button1: FloatingActionButton, imageView: ImageView) {
        viewModelScope.launch {

            tenantRepository.getTenantCount().collect {

                if (it == 0) {
                    button1.visibility = View.GONE
                    button.visibility = View.VISIBLE
                    imageView.visibility = View.VISIBLE
                } else {
                    button1.visibility = View.VISIBLE
                    button.visibility = View.GONE
                    imageView.visibility = View.GONE
                }
            }
        }
    }
}
