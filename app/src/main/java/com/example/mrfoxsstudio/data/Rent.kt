package com.example.mrfoxsstudio.data

import android.widget.CheckBox
import androidx.room.*
import java.util.*

@Entity(
    tableName = "rents",
    indices = [],
    foreignKeys = [ForeignKey(
        entity = Property::class,
        parentColumns = ["id"],
        childColumns = ["id"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class Rent(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) val propertyId: Long,
    val rent: String,
    @ColumnInfo(name = "rent_due") val rentDue: Calendar,
    @ColumnInfo(name = "rent_end") val rentEnd: Calendar,
    val rented: Boolean,
    val currency: String
)