package com.example.mrfoxsstudio.data

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TenantRepository @Inject constructor(
    private val tenantDao: TenantDao
) {
    suspend fun createTenant(name: String, propertyId: Long, email: String, phone: String) {
        val tenant = Tenant(name, email, phone, propertyId)
        tenantDao.insertTenant(tenant)
    }

    fun getPropertiesWithTenants() = tenantDao.getPropertiesWithTenants();
    fun getTenant(tenantId: Long) = tenantDao.getTenant(tenantId)
    fun getTenants(propertyId: Long) = tenantDao.getTenants(propertyId)

    fun getTenantCount() = tenantDao.getTenantCount()

    suspend fun removeTenant(tenant: Tenant) {
        tenantDao.deleteTenant(tenant)
    }

    suspend fun updateTenant(tenant: Tenant) {
        tenantDao.updateTenant(tenant)
    }
}
