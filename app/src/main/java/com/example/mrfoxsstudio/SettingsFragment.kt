package com.example.mrfoxsstudio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mrfoxsstudio.databinding.FragmentSettingsBinding
import com.example.mrfoxsstudio.viewmodels.SettingsViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsFragment : Fragment() {
    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate<FragmentSettingsBinding>(
            inflater,
            R.layout.fragment_settings,
            container,
            false
        ).apply {
            viewModel = this@SettingsFragment.viewModel
            lifecycleOwner = viewLifecycleOwner

            topAppBar.setNavigationOnClickListener {
                drawerLayout.open()
            }

            navView.setNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.nav_properties -> findNavController().navigate(R.id.action_settings_fragment_to_property_list_fragment)
                    R.id.nav_profile -> drawerLayout.close()
                }
                true
            }

            buttonDeleteAll.setOnClickListener()
            {
                view?.let { it1 -> showAlertDialog(it1) }
            }
        }
        return binding.root
    }

    fun showAlertDialog(view: View) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Delete")
            .setMessage(
                "Are you sure you want to delete all the data in the application? " +
                        "This means you will lose all your properties, tenants and rent details."
            )
            .setNegativeButton("No") { dialog, which ->
                showSnackbar("The data was not deleted", view)
            }
            .setPositiveButton("Yes") { dialog, which ->
                viewModel.onClick(view)
            }.show()
    }

    fun showSnackbar(msg: String, view: View) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}