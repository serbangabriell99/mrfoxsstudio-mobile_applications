package com.example.mrfoxsstudio.viewmodels

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.PropertyDetailFragmentArgs
import com.example.mrfoxsstudio.PropertyEditFragmentArgs
import com.example.mrfoxsstudio.R
import com.example.mrfoxsstudio.data.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PropertyEditViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val appDatabase: AppDatabase,
    private val propertyRepository: PropertyRepository
) : ViewModel() {
    val propertyId: Long = savedStateHandle.get<Long>(PROPERTY_ID_SAVED_STATE_KEY)!!

    private val _address = MutableLiveData<String>()
    val address: LiveData<String> get() = _address

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> get() = _name

    private val _postcode = MutableLiveData<String>()
    val postcode: LiveData<String> get() = _postcode

    private val _type = MutableLiveData<String>()
    val type: LiveData<String> get() = _type


    init {
        if (propertyId != -1L) {
            viewModelScope.launch {
                propertyRepository.getProperty(propertyId).collect { property ->
                    if (property != null) {
                        _address.postValue(property.address)
                        _name.postValue(property.name)
                        _type.postValue(property.type)
                        _postcode.postValue(property.postcode)
                    }

                }
            }
        }
    }

    fun onSaveClick(view: View) {
        viewModelScope.launch {
            if (_name.value == null || _name.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Property Name", 1000)
                mySnackbar.show()
                return@launch
            }


            if (_type.value == null) {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Property Type", 1000)
                mySnackbar.show()
                return@launch
            }

            if (_address.value == null || _address.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please pick a Property Address", 1000)
                mySnackbar.show()
                return@launch
            }

            if (_postcode.value == null || _postcode.value == "") {
                hideKeyboard(view)
                val mySnackbar = Snackbar.make(view, "Please add Property Postcode", 1000)
                mySnackbar.show()
                return@launch
            }

            val property = Property(
                _address.value!!,
                _name.value!!,
                _type.value!!,
                _postcode.value!!,
            )
            if (propertyId == -1L) {
                propertyRepository.createProperty(
                    property.postcode,
                    property.address,
                    property.name,
                    property.type
                )
            } else {
                propertyRepository.updateProperty(
                    property.also { it.propertyId = propertyId })
            }
            view.findNavController().popBackStack()


            hideKeyboard(view)

        }
    }

    fun onAddressChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _address.postValue(s.toString())
    }

    fun onNameChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _name.postValue(s.toString())
    }

    fun onPostcodeChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _postcode.postValue(s.toString())
    }

    fun onTypeChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _type.postValue(s.toString())
    }

    fun onDeleteProperty(view: View) {
        viewModelScope.launch {
            if (propertyId != -1L) {
                propertyRepository.removeProperty(
                    Property(
                        "",
                        "",
                        "",
                        "",
                    ).also { it.propertyId = propertyId })
                view.findNavController().popBackStack(R.id.property_list_fragment, false)
                hideKeyboard(view)
            }
        }

    }

    fun hideKeyboard(view: View) {
        val imm =
            view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    companion object {
        private const val PROPERTY_ID_SAVED_STATE_KEY = "propertyId"
    }
}
