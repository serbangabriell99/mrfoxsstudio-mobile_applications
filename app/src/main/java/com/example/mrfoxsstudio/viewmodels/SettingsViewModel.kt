package com.example.mrfoxsstudio.viewmodels

import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.example.mrfoxsstudio.PropertyDetailFragmentDirections
import com.example.mrfoxsstudio.data.AppDatabase
import com.example.mrfoxsstudio.data.PropertyRepository
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val appDatabase: AppDatabase,
) : ViewModel() {

    fun onClick(view: View) {
        viewModelScope.launch(Dispatchers.IO) {
            appDatabase.clearAllTables()
            val mySnackbar = Snackbar.make(view, "Properties Deleted", 1000)
            mySnackbar.show()
        }
    }
}