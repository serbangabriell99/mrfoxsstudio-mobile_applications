package com.example.mrfoxsstudio.data

import androidx.room.Embedded
import androidx.room.Relation

data class PropertyWithTenants(
    @Embedded val property: Property,
    @Relation(
        parentColumn = "id",
        entityColumn = "property_id"
    )
    val tenants: List<Tenant>
)
